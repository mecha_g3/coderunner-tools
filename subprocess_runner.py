# -*- coding: utf-8 -*-

import subprocess
import shlex

def run(args):
    command = shlex.split(args)
    proc = subprocess.Popen(command,
                            stdin=subprocess.PIPE,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            shell=False)
    proc.wait()
    output = []
    for line in proc.stdout.readlines():
        output += line.split()
    return output

if __name__ == '__main__':
    print run("ls -al")
