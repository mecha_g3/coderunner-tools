# -*- coding: utf-8 -*-
import urllib
import urllib2
import time
import unittest
import logging
import json
import os
import tempfile

class APIRequester:
    log_path = "API_LOG.txt"
    def __init__(self, base_url, token):
        self.logger = logging.getLogger("API")
        self.base_url = base_url
        self.token = token
        self.cache = {}
        self.cache_enable = False
        self.last_cached = False

    def append_cache(self, request_str, response_str):
        if self.enable_cache:
            self.cache[request_str] = response_str

    def enable_cache(self, enable = True):
        if enable and os.path.isfile(self.log_path):
            with open(self.log_path , "r") as apilog:
                for log in apilog:
                    log_json = json.loads(log)
                    self.append_cache(log_json["request"], log_json["response"])
        self.cache_enable = enable

    def get_uri(self, param = None):
        p = {}
        if self.token:
            p.update({'token': self.token})
        if param:
            p.update(param)
        if p:
            return self.base_url + '?' + urllib.urlencode(p)
        else:
            return self.base_url

    def send(self, param = None):
        result = urllib2.urlopen(self.get_uri(param))
        response = result.read()
        if result and result.getcode() == 200:
            with open(self.log_path, "a") as apilog:
                log = json.dumps({
                    'time': time.strftime("%Y-%m-%d %H:%M:%S"),
                    'request' : result.geturl(),
                    'response' : response,
                }, ensure_ascii = False)
                print >>apilog, log
            self.append_cache(result.geturl(), response)
        return response, result.getcode()

    def request(self, param = None):
        u"""
        サーバが死んでも, 通信が途絶えても, プログラムは殺さない
        """
        try:
            if self.cache_enable and self.get_uri(param) in self.cache:
                self.last_cached = True
                return self.cache[self.get_uri(param)]
            self.last_cached = False
            response = self._request(param)
            if response:
                return response 
            return False
        except Exception, err:
            self.logger.error(err)
            return False
        return False

    def _request(self, param = None):
        raise NotImplementedError

class SimpleSleepAPIRequester(APIRequester):
    u"""
    保険用.
    受信してから1秒経過後に投げる
    """
    def __init__(self, base_url, token, interval = 1.0):
        APIRequester.__init__(self, base_url, token)
        self.interval = interval
        self.last_recv_time = 0

    def _sleep(self):
        elapsed_time = time.time() - self.last_recv_time
        should_sleep_time = self.interval - elapsed_time
        if should_sleep_time > 0:
            time.sleep(should_sleep_time)

    def _request(self, param = None):
        self._sleep()
        response, code = self.send(param)
        if code == 200:
            self.last_recv_time = time.time()
            return response
        return False


class DynamicSleepAPIRequester(APIRequester):
    u"""
    間隔が短すぎてエラーになった場合にも1秒制限がかかる場合用.
    動的な時間Sleepを挟む.
    1秒というのが, サーバが受理してから1秒なのか, 処理が終わってから1秒なのか.
    仕様が不明なのでいざという時には dynamic_eps_min で微調整する.
    """
    def __init__(self, base_url, token, interval = 1.0):
        APIRequester.__init__(self, base_url, token)
        self.interval = interval
        self.last_send_time = 0
        self.last_dynamic_sleep_time = 0
        self.dynamic_eps = 0.0
        self.dynamic_eps_min = 0.1

    def _dynamic_sleep(self):
        elapsed_time = time.time() - self.last_send_time
        should_sleep_time = max(0.0, self.interval - elapsed_time + self.dynamic_eps)
        should_sleep_time = min(should_sleep_time, self.interval)
        self.last_dynamic_sleep_time = should_sleep_time
        if should_sleep_time > 0.0 :
            time.sleep(should_sleep_time)

    def _request(self, param = None):
        self._dynamic_sleep()
        send_time = time.time()
        response, code = self.send(param)
        if code == 200:
            self.last_send_time = send_time
            self.dynamic_eps = max(0, self.dynamic_eps - 0.01)
            return response
        if code == 400 and response == "Too Frequent Access":
            self.dynamic_eps += 0.05
        return False

class DosAPIRequester(APIRequester):
    u"""
    API制限が無い場合と, 間隔が短すぎてエラーになった場合に1秒制限がかからない場合用.
    とにかく投げまくる. 50ms間隔くらいなら怒られないだろう.
    TCP Connection張るのにも時間かかるしね.
    """
    def __init__(self, base_url, token, interval = 0.05, timeout = 1.5):
        APIRequester.__init__(self, base_url, token)
        self.interval = interval
        self.timeout = timeout

    def _request(self, param = None):
        start_time = time.time()
        while(time.time() - start_time < self.timeout):
            response, code = self.send(param)
            if code == 200:
                return response
            elif code == 400 and response == "Too Frequent Access":
                time.sleep(self.interval)
                continue
            break
        return False

### UNIT TEST ###

class MockResult:
    def __init__(self, url, code, result):
        self.url = url
        self.code = code
        self.result = result

    def read(self):
        return self.result

    def geturl(self):
        return self.url

    def getcode(self):
        return self.code

class MockServer:
    def __init__(self, url, code = 200, result = "Message", response_time = 0.1):
        self.response_time = response_time
        self.mock = MockResult(url, code, result)
        self.__backup = urllib2.urlopen

    def __enter__(self):
        def mock_urlopen(url, data = None, timeout = None, cafile = None, capath = None, cadefault = None, context = None):
            time.sleep(self.response_time)
            return self.mock
        urllib2.urlopen = mock_urlopen

    def __exit__(self, type, value, traceback):
        urllib2.urlopen = self.__backup

class APIRequesterTestCase(unittest.TestCase):
    def test_get_uri(self):
        requester = DynamicSleepAPIRequester("http://example.com/api", "mytoken")
        self.assertEqual(requester.get_uri(), "http://example.com/api?token=mytoken")
        self.assertEqual(requester.get_uri({"hogekey" : "hogevalue"}), "http://example.com/api?token=mytoken&hogekey=hogevalue")

    def test_simple_sleep(self):
        requester = SimpleSleepAPIRequester("http://example.com", "mytoken", 0)
        with MockServer(requester.get_uri(), 200, "OK", 0):
            self.assertEqual(requester.request(), "OK")
        with MockServer(requester.get_uri(), 400, "Too Frequent Access", 0):
            self.assertEqual(requester.request(), False)
        with MockServer(requester.get_uri(), 200, "OK", 0):
            self.assertEqual(requester.request(), "OK")
        with MockServer(requester.get_uri(), 123, "BAD", 0):
            self.assertEqual(requester.request(), False)
        with MockServer(requester.get_uri(), 400, "Too Frequent Access", 0):
            self.assertEqual(requester.request(), False)
        with MockServer(requester.get_uri(), 200, "OK", 0):
            self.assertEqual(requester.request(), "OK")

    def test_dynamic_sleep(self):
        requester = DynamicSleepAPIRequester("http://example.com", "mytoken", 0)
        with MockServer(requester.get_uri(), 200, "OK", 0):
            self.assertEqual(requester.request(), "OK")
        with MockServer(requester.get_uri(), 400, "Too Frequent Access", 0):
            self.assertEqual(requester.request(), False)
        with MockServer(requester.get_uri(), 200, "OK", 0):
            self.assertEqual(requester.request(), "OK")
        with MockServer(requester.get_uri(), 123, "BAD", 0):
            self.assertEqual(requester.request(), False)
        with MockServer(requester.get_uri(), 400, "Too Frequent Access", 0):
            self.assertEqual(requester.request(), False)
        with MockServer(requester.get_uri(), 200, "OK", 0):
            self.assertEqual(requester.request(), "OK")

    def test_dos(self):
        requester = DosAPIRequester("http://example.com", "mytoken", timeout = 0.1)
        with MockServer(requester.get_uri(), 200, "OK", 0):
            self.assertEqual(requester.request(), "OK")
        with MockServer(requester.get_uri(), 400, "Too Frequent Access", 0):
            self.assertEqual(requester.request(), False)
        with MockServer(requester.get_uri(), 200, "OK", 0):
            self.assertEqual(requester.request(), "OK")
        with MockServer(requester.get_uri(), 123, "BAD", 0):
            self.assertEqual(requester.request(), False)
        with MockServer(requester.get_uri(), 400, "Too Frequent Access", 0):
            self.assertEqual(requester.request(), False)
        with MockServer(requester.get_uri(), 200, "OK", 0):
            self.assertEqual(requester.request(), "OK")

    def test_dynamic_sleep_time(self):
        requester = DynamicSleepAPIRequester("http://example.com", "mytoken", 0.1)
        with MockServer(requester.get_uri(), 200, "OK", 0):
            requester.request()
            self.assertEqual(requester.last_dynamic_sleep_time, 0)
            requester.request()
            self.assertTrue(0.09 < requester.last_dynamic_sleep_time <= 0.1)
            requester.request()
            self.assertTrue(0.09 < requester.last_dynamic_sleep_time <= 0.1)
            with MockServer(requester.get_uri(), 400, "Too Frequent Access", 0):
                requester.request()
                self.assertTrue(0.09 < requester.last_dynamic_sleep_time <= 0.1)
            requester.request()
            self.assertTrue(requester.last_dynamic_sleep_time, 0)
            with MockServer(requester.get_uri(), 123, "BAD", 0):
                requester.request()
                self.assertTrue(0.09 < requester.last_dynamic_sleep_time <= 0.1)
            requester.request()
            self.assertTrue(requester.last_dynamic_sleep_time, 0)

    def test_cache(self):
        tmp = tempfile.NamedTemporaryFile()
        APIRequester.log_path = tmp.name
        requester = DynamicSleepAPIRequester("http://example.com", "mytoken", 0.1)
        requester.enable_cache()
        param1 = {"hoge" : "vhoge", "piyo" : "vpiyo"}
        param2 = {"piyo" : "vpiyo", "hoge" : "vhoge"}
        param3 = {"PIYO" : "vpiyo", "hoge" : "vhoge"}

        with MockServer(requester.get_uri(param1), 200, "OK", 0.1):
            requester.request(param1)
            self.assertFalse(requester.last_cached)

        with MockServer(requester.get_uri(param2), 200, "OK", 0.1):
            requester.request(param2)
            self.assertTrue(requester.last_cached)

        with MockServer(requester.get_uri(param3), 200, "OK", 0.1):
            requester.request(param3)
            self.assertFalse(requester.last_cached)

        with MockServer(requester.get_uri(param3), 200, "OK", 0.1):
            requester.request(param3)
            self.assertTrue(requester.last_cached)
        tmp.close()

if __name__ == "__main__":
    unittest.main()

