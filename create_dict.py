# -*- coding: utf-8 -*-
import requester
import random
import os

class Solver:
    def __init__(self):
        self.dict_score = {}
        self.requester = requester.SimpleSleepAPIRequester("http://mecha-g3.com:10000/q", "OD6BTL4FMMIAPDVCLQ6D", 1)
        self.requester.enable_cache(True)
        if os.path.isfile("dict.txt"):
            with open("dict.txt", "r") as f:
                for line in f:
                    line = line.split()
                    self.dict_score[line[0]] = int(line[1])

    def estimate_score(self, text):
        score = 0
        for k, v in self.dict_score.items():
            score += self.count_str(text, k) * v
        return score

    def count_str(self, src, target):
        ret = 0
        for i in xrange(len(src)):
            if src.startswith(target, i):
                ret += 1
        return ret

    def generate_str(self, length, chars):
        return ''.join(random.choice(chars) for _ in range(length))

    def create_dict(self):
        s = "ABCD"

        for a in xrange(4):
            text = s[a]
            expected = self.estimate_score(text)
            score = self.ask(text)
            if expected < score:
                self.dict_score[text] = score - expected

        for a in xrange(4):
            for b in xrange(4):
                text = s[a] + s[b]
                expected = self.estimate_score(text)
                score = self.ask(text)
                if expected < score:
                    self.dict_score[text] = score - expected

        for a in xrange(4):
            for b in xrange(4):
                for c in xrange(4):
                    text = s[a] + s[b] + s[c]
                    expected = self.estimate_score(text)
                    score = self.ask(text)
                    if expected < score:
                        self.dict_score[text] = score - expected

        for a in xrange(4):
            for b in xrange(4):
                for c in xrange(4):
                    for d in xrange(4):
                        text = s[a] + s[b] + s[c] + s[d]
                        expected = self.estimate_score(text)
                        score = self.ask(text)
                        if expected < score:
                            self.dict_score[text] = score - expected

        for a in xrange(4):
            for b in xrange(4):
                for c in xrange(4):
                    for d in xrange(4):
                        for e in xrange(4):
                            text = s[a] + s[b] + s[c] + s[d] + s[e]
                            expected = self.estimate_score(text)
                            score = self.ask(text)
                            if expected < score:
                                self.dict_score[text] = score - expected

        print self.dict_score
        print "Dict size :", len(self.dict_score)

        while(True):
            random_str = self.generate_str(50, "ABCD")
            expected = self.estimate_score(random_str)
            score = self.ask(random_str)
            print "expected", expected
            print "score", score

            if score < expected:
                print "[WARN] score < expected:"
                assert(False)
                continue
            if score == expected:
                continue

            #2分探索
            L = 0
            R = len(random_str)
            M = L + (R - L) / 2
            while(True):
                print "LRM", L, R, M
                sub_str_l = random_str[L:M]
                expected_l = self.estimate_score(sub_str_l)
                score_l = self.ask(sub_str_l)

                sub_str_r = random_str[M:R]
                expected_r = self.estimate_score(sub_str_r)
                score_r = self.ask(sub_str_r)

                if score_l > expected_l:
                    R = M
                    M = L + (R - L) / 2
                elif  score_r > expected_r:
                    L = M
                    M = L + (R - L) / 2
                else:
                    break
            print "END BINARY SERCH", "LR", L, R

            print "LR", L, R
            print random_str[L:R]
            #ここから線形探索
            for i in xrange(R - L):
                sub_str = random_str[L+1:R]
                expected = self.estimate_score(sub_str)
                score = self.ask(sub_str)
                if score > expected:
                    L += 1
                else:
                    break

            for i in xrange(R - L):
                sub_str = random_str[L:R-1]
                expected = self.estimate_score(sub_str)
                score = self.ask(sub_str)
                if score > expected:
                    R -= 1
                else:
                    break

            expected = self.estimate_score(random_str[L:R])
            score = self.ask(random_str[L:R])
            if score > expected:
                self.dict_score[random_str[L:R]] = score - expected
                print "found", random_str[L:R], score - expected
                with open("dict.txt", "w") as f:
                    for k, v in self.dict_score.items():
                        print >>f, k, v

    def ask(self, text):
        score = int(self.requester.request({"str" : text}))
        print text, score
        return score


if __name__ == '__main__':
    solver = Solver()
    solver.create_dict()

