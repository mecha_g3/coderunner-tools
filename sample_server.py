# -*- coding: utf-8 -*-
import SimpleHTTPServer
import SocketServer
import urlparse
import urllib
import datetime
import random
import sqlite3
import string
import time
import cgi

#https://game.coderunner.jp/q?token=CD06U1G5AP3J1RHO5K1WK7XXUWUJ833X&str=AAAAAAA

PORT = 10000
db = sqlite3.connect("data.db")
db.text_factory = str

def generate_str(length, chars):
    return ''.join(random.choice(chars) for _ in range(length))

def generate_dict():
    ret = {}
    base_score = 10
    while(len(ret) < 1000):
        length = random.randint(1, 8)
        score = length**3 * base_score
        ret[generate_str(length, "ABCD")] = score
    return ret

def init_database():
    cur = db.cursor()
    sql = u"""
    CREATE TABLE IF NOT EXISTS users (
            user_id INTEGER PRIMARY KEY AUTOINCREMENT,
            user_name TEXT,
            token TEXT,
            comment TEXT,
            max_score TEXT,
            timestamp TEXT
            );"""
    cur.execute(sql)
    db.commit()

def add_user(name, token):
    cur = db.cursor()
    time_str = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    sql = u'INSERT INTO users (user_name, token, comment, max_score, timestamp) VALUES (?, ?, ?, ?, ?);'
    cur.execute(sql, (name, token, "", "0", time_str))
    db.commit()

def get_ranking():
    cur = db.cursor()
    sql = u'SELECT token, user_name, max_score, comment FROM users;'
    cur.execute(sql)
    result = []
    for row in cur.fetchall():
        result.append([row[0], row[1], int(row[2]), row[3]])
    result.sort(key=lambda x:x[2], reverse=True)
    return result

def get_score_by_token(token):
    cur = db.cursor()
    sql = u'SELECT max_score FROM users WHERE token = ?;'
    cur.execute(sql, (token,))
    result = int(cur.fetchone()[0])
    return result

def update_score(token, score):
    cur = db.cursor()
    sql = u'UPDATE users SET max_score = ? WHERE token = ?;'
    cur.execute(sql, (str(score), token))
    db.commit()

def update_comment(token, comment):
    cur = db.cursor()
    sql = u'UPDATE users SET comment = ? WHERE token = ?;'
    cur.execute(sql, (str(comment), token))
    db.commit()

def count_str(src, target):
    ret = 0
    for i in xrange(len(src)):
        if src.startswith(target, i):
            ret += 1
    return ret

def load_tokens():
    cur = db.cursor()
    sql = u'SELECT token FROM users;'
    cur.execute(sql)
    result = []
    for row in cur.fetchall():
        result += row
    return result

class SampleServerHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    dict_score = None
    token_list = []
    last_request_dict = {}
    last_profile_dict = {}
    token_max_score = {}

    def do_GET(self):
        if self.path.startswith('/register'):
            parsed = urlparse.urlparse(self.path)
            query_dict = dict(urlparse.parse_qsl(parsed.query))
            if "name" not in query_dict:
                self.send_response(400)
                self.send_header("Content-type", "text/plain")
                self.end_headers()
                self.wfile.write("Invalid Name")
                return

            name = query_dict["name"]
            token = generate_str(20, string.ascii_uppercase + string.digits)
            add_user(name, token)
            SampleServerHandler.token_list = load_tokens()
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write(token)
            print query_dict, token
            return

        if self.path.startswith('/ranking'):
            rank_list = get_ranking()
            self.send_response(200)
            self.send_header("Content-type", "text/html")
            self.end_headers()
            self.wfile.write("""<!DOCTYPE html> <html lang="ja"> <head> <meta charset="UTF-8"> <title lang="en">ranking</title> </head> <body>""")
            self.wfile.write("""<table>""")
            self.wfile.write("""<tr> <th>user_name</th> <th>max_score</th> <th>comment</th> </tr>""")
            for user in rank_list:
                if user[2] > 0:
                    self.wfile.write("""<tr> <td>%s</td> <td>%s</td> <td>%s</td> </tr>""" % (cgi.escape(user[1]), str(user[2]), cgi.escape(user[3])))
            self.wfile.write("""</table>""")
            self.wfile.write("""</body></html>""")
            return

        if self.path.startswith('/profile'):
            try:
                parsed = urlparse.urlparse(self.path)
                query_dict = dict(urlparse.parse_qsl(parsed.query))
                print query_dict
                if "token" not in query_dict:
                    self.send_response(400)
                    self.send_header("Content-type", "text/plain")
                    self.end_headers()
                    self.wfile.write("Invalid Token")
                    return

                if query_dict["token"] not in self.token_list:
                    self.send_response(400)
                    self.send_header("Content-type", "text/plain")
                    self.end_headers()
                    self.wfile.write("Invalid Token")
                    return

                if "text" not in query_dict:
                    self.send_response(400)
                    self.send_header("Content-type", "text/plain")
                    self.end_headers()
                    self.wfile.write("Invalid Text")
                    return

                token = query_dict["token"]
                print query_dict["text"]
                text = query_dict["text"]
                if token in self.last_profile_dict:
                    t = self.last_profile_dict[token]
                    if time.time() - t < 1.0:
                        self.send_response(200)
                        self.send_header("Content-type", "text/plain")
                        self.end_headers()
                        self.wfile.write("OK")
                        return

                self.last_profile_dict[token] = time.time()
                update_comment(token, text)

            except Exception, err:
                print err
                self.send_response(400)
                self.send_header("Content-type", "text/plain")
                self.end_headers()
                self.wfile.write("-1")

        if self.path.startswith('/q'):
            try:
                parsed = urlparse.urlparse(self.path)
                query_dict = dict(urlparse.parse_qsl(parsed.query))
                print query_dict
                if "token" not in query_dict:
                    self.send_response(400)
                    self.send_header("Content-type", "text/plain")
                    self.end_headers()
                    self.wfile.write("Invalid Token")
                    return

                if query_dict["token"] not in self.token_list:
                    self.send_response(400)
                    self.send_header("Content-type", "text/plain")
                    self.end_headers()
                    self.wfile.write("Invalid Token")
                    return

                if "str" not in query_dict:
                    self.send_response(400)
                    self.send_header("Content-type", "text/plain")
                    self.end_headers()
                    self.wfile.write("Invalid String")
                    return

                token = query_dict["token"]
                s = query_dict["str"]

                if not 1 <= len(s) <= 50:
                    self.send_response(400)
                    self.send_header("Content-type", "text/plain")
                    self.end_headers()
                    self.wfile.write("Invalid String")
                    return

                valid_str = True
                for c in s:
                    if not ord('A') <= ord(c) <= ord('Z'):
                        valid_str = False
                        break

                if not valid_str:
                    self.send_response(400)
                    self.send_header("Content-type", "text/plain")
                    self.end_headers()
                    self.wfile.write("Invalid String")
                    return

                if token in self.last_request_dict:
                    t = self.last_request_dict[token]
                    if time.time() - t < 1.0:
                        self.send_response(400)
                        self.send_header("Content-type", "text/plain")
                        self.end_headers()
                        self.wfile.write("Too Frequent Access")
                        return

                self.last_request_dict[token] = time.time()

                score = 0
                for text, bonus in self.dict_score.items():
                    score += count_str(s, text) * bonus

                print "score:", score
                max_score = get_score_by_token(token)
                if not max_score or max_score < score:
                    update_score(token, score)

                self.send_response(200)
                self.send_header("Content-type", "text/plain")
                self.end_headers()
                self.wfile.write(str(score))

            except Exception, err:
                print err
                self.send_response(400)
                self.send_header("Content-type", "text/plain")
                self.end_headers()
                self.wfile.write("-1")


if __name__ == '__main__':
    init_database()
    random.seed(123)
    SampleServerHandler.token_list = load_tokens()
    print SampleServerHandler.token_list
    SampleServerHandler.dict_score = generate_dict()
    print SampleServerHandler.dict_score
    httpd = SocketServer.TCPServer(("", PORT), SampleServerHandler)
    print "serving at port", PORT
    httpd.serve_forever()
