# -*- coding: utf-8 -*-
import os
import requester
import json

class APILogParser:
    def __init__(self):
        pass

    def parse(self):
        if os.path.isfile(requester.APIRequester.log_path):
            all_log = None
            with open(requester.APIRequester.log_path, "r") as logfile:
                all_log = logfile.readlines()
            for log in all_log:
                json_log = json.loads(log)
                print json_log['request']
                print json_log['response']
                print json_log['time']
                print json.dumps(json_log, indent=4)

if __name__ == '__main__':
    APILogParser().parse()
