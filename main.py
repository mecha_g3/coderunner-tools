# -*- coding: utf-8 -*-
import logging
import requester
import random
import json

token = "Q9blMzFWz2rorJzxyWgYWwfPhWJFTQXs"

sub_tokens = [
    "2ahyTtYPiUgbDjYL9K5kCNiMLFW5zkrC",
    "tW21qYFrQQHe5m0nHA9bf3OdUkiT7UGp",
    "epWTXk9Enfmashtavvi8ldqfJim9mPHZ",
    "cVtNnMVwuoLyQTvQXbcz4UCo7xOlY8wo",
    "Lz1IeFj2kci6IOeOHEUkcJzAsDqIKBmi",
    "32Tmb4uO42RmHq4ibtxeDaCWaRMnvvkO",
    "oaOdlJSicrUhfJ3V9xkv7vgCuqz2Wvs9",
    "IBEBAC215PVAzeuHplZT0hPqEBuFDIDw",
    "KwPLgmtq86lIjkX6qJhwXQRMAOH6YxPC",
    "Z7eQiyMGJzKUgXdkkB9wzyO6FrNeIGk4",
    "p1OTppDunGIg9xfggdu6GF35tpiU4dUQ",
    "fXbKzyN4iSbQkNdaY7gjp2jPHW7ohsad",
    "FfY4SC5qP1h3k36R1UUN2EMLSUsBLhbG",
    "jjplspt8FEF5EvdE3NApdb8e0bXrIrAN",
    "tF8Ml0sZvHMO3L1sUgJMIW5Jm3471OiC",
    "V92YFICh1FmGZFTpHP9TOEpKbZ5ds62k",
    "ovuxot3GPbFFlFa6cj8VUcsVKwhejrg4",
    "LXUKXPImKCJUEWJahy9fyOpyLd9YpWYG",
    "IWbneePn8nAYsb9etPZghXD59OLjkfvA",
    "NigysBgKI8TMexoQJoohESi3fQZy9VR1",
    "ELnehRZ5fxI5V9davjroPcwU5oQxHCvm",
    "MGYVRB4hbPuRdwIvMiW8EA89ygdhd9ow",
    "Owpbzak5mK3nsfxDhwFkbuPgxHfzVLKW",
]

summon = requester.SimpleSleepAPIRequester("http://game-client.coderunner.jp/summon", token, 1)
trade = requester.SimpleSleepAPIRequester("http://game-client.coderunner.jp/trade", token, 1)
info = requester.SimpleSleepAPIRequester("http://game-client.coderunner.jp/info.json", token, 1)
scorelist = requester.SimpleSleepAPIRequester("http://game-client.coderunner.jp/scorelist", token, 1)
market = requester.SimpleSleepAPIRequester("http://game-client.coderunner.jp/market.json", token, 1)
profile = requester.SimpleSleepAPIRequester("http://game-client.coderunner.jp/profile", token, 1)

def need_rare():
    for i in xrange(5):
        trade.request({"in": 20 + i, "out" : i, "num" : 999})
        trade.request({"in": 20 + i, "out" : 5 + i, "num" : 999})
        trade.request({"in": 20 + i, "out" : 10 + i, "num" : 999})

def bad_trade():
    for t in sub_tokens:
        inf = requester.SimpleSleepAPIRequester("http://game-client.coderunner.jp/info.json", t, 1).request()
        if inf:
            inf = json.loads(inf)
            rare = inf["stone"][20:]
            trd = requester.SimpleSleepAPIRequester("http://game-client.coderunner.jp/trade", t, 1)
#もってるか判定
            for i in xrange(5):
                if rare[i]:
                    trd.request({"in": i, "out" : 20 + i, "num" : rare[i]})

def do_summon():
    scores = scorelist.request()
    score_list = scores.split("\r\n")
    print score_list
    f = open("monsters.json", "r")
    monsters = json.load(f)
    f.close()

    print len(monsters)
    monsters = monsters[:50]
    for i in xrange(50):
        monsters[i]["baseScore"] = int(score_list[i])
    monsters.sort(key=lambda m: m["baseScore"], reverse=True)

    me = json.loads(info.request())
    print me["stone"]
    for monster in monsters:
        ok = True
        for i in xrange(len(monster["required"])):
            if monster["required"][i] > me["stone"][i]:
                ok = False
        if ok:
            for i in xrange(10):
                print summon.request({"monster": monster["id"]})
            me = json.loads(info.request())

def gomen():
    for t in sub_tokens:
        p = requester.SimpleSleepAPIRequester("http://game-client.coderunner.jp/profile", t, 1)
        p.request({"text": "ごめんなさい"})

if __name__ == '__main__':
    need_rare()
    profile.request({"text": "不正はなかった"})
    gomen()
    print info.request()
    for i in xrange(100):
        bad_trade()
        for _ in xrange(100):
            do_summon()
            print info.request()
