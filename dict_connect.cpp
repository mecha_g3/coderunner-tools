#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <algorithm>
#include <fstream>
using namespace std;

vector<pair<int, string> > scores;

int count_str(const string& src, const string& target) {
    int ret = 0;
    for (int i = 0; i < (int)src.size() - (int)target.size() + 1; ++i) {
        if (src.compare(i, target.size(), target) == 0) {
            ret += 1;
        }
    }
    return ret;
}

int estimate_score(const string& text) {
    int score = 0;
    for (const auto& p : scores) {
        score += p.first * count_str(text, p.second);
    }
    return score;
}

int main() {
    int score;
    string str;
    ifstream ifs("dict.txt");

    while (ifs >> str >> score) {
        scores.emplace_back(score, str);
    }
    sort(scores.begin(), scores.end());

    vector<int> dpscr(51);
    vector<string> dpstr(51);

    for (int i = 0; i < 51; ++i) {
        dpstr[i] = string(i, 'A');
    }

    for (int i = 0; i < 51; ++i) {
        for (const auto& p : scores) {
            const string text = p.second;
            const int k = i - (int)text.size();
            if (k >= 0) {
                const auto target = dpstr[k] + text;
                const auto escore = estimate_score(target);
                if (dpscr[i] < escore) {
                    dpscr[i] = escore;
                    dpstr[i] = target;
                }
            }
        }
        cout << dpscr[i] << endl;
        cout << dpstr[i] << endl;
    }

    cout << dpscr[50] << endl;
    cout << dpstr[50] << endl;
}
